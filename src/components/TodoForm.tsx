import React, { FC, useState } from 'react';
import { Form } from 'react-bootstrap';

const ToDoForm: FC<any> = ({ addTodo }: any) => {
  const [value, setValue] = useState("");
  const name = sessionStorage.getItem('user_name');

  const handleSubmit = (e: any) => {
    e.preventDefault();
    if (!value) return;
    addTodo(value);
    setValue("");
  };

  return (
    <div className="card text-center mb-2">
      <div className="card-body">
        <h5 className="card-title text-left" style={{ paddingLeft: '15px' }}>
          Welcome <span className="text-capitalize" style={{fontSize: '1.3rem'}}> {name} </span>
        </h5>

        <div className="row m-auto">

          <div className="col-10">
            <Form onSubmit={handleSubmit}>
              <Form.Group controlId="formBasicEmail">
                <Form.Control
                  type="text"
                  className="input"
                  value={value}
                  onChange={e => setValue(e.target.value)} />
              </Form.Group>
            </Form>
          </div>

          <div className="col-2">
            <i className="fa fa-plus-square plus-icon" role="button" onClick={handleSubmit} aria-hidden="true" />
          </div>
        </div>

      </div>
    </div>
  )
}
export default ToDoForm;
