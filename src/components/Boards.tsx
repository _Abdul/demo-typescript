import React, { FC, useState } from 'react';
import TodoForm from './TodoForm';
import Todo from './ToDo';
import ReactDragListView from 'react-drag-listview/lib/index.js';
import { logout } from '../isAuthenticated';
import { useHistory } from 'react-router';

const Boards: FC<any> = () => {
  const history = useHistory();
  const [todos, setTodos] = useState([
    {
      text: "Learn about React",
      isCompleted: false
    },
    {
      text: "Learn Node js",
      isCompleted: false
    },
    {
      text: "Learn Typescript",
      isCompleted: false
    }
  ]);

  const dragProps = {
    onDragEnd(fromIndex, toIndex) {
      const data = [...todos];
      const item = data.splice(fromIndex, 1)[0];
      data.splice(toIndex, 0, item);
      setTodos(data);
      let element = document.querySelector('.todo.addSpace');
      element?.classList.remove('addSpace');
    },
    nodeSelector: 'div',
    handleSelector: 'div',
    lineClassName: 'drag-line'
  };

  const addTodo = (text: any) => {
    const newTodos: any = [...todos, { text }];
    setTodos(newTodos);
  };

  const removeTodo = (index: number) => {
    const newTodos = [...todos];
    newTodos.splice(index, 1);
    setTodos(newTodos);
  };

  const completeTodo = (index: number) => {
    const newTodos = [...todos];
    newTodos[index].isCompleted = newTodos[index].isCompleted === true ? false : true;
    setTodos(newTodos);
  };

  return (
      <div className="todo-list">
        <TodoForm addTodo={addTodo} />
        <ReactDragListView {...dragProps}>
          {todos.map((todo, index) => (
            <Todo
              key={index}
              index={index}
              todo={todo}
              completeTodo={completeTodo}
              removeTodo={removeTodo}
            />
          ))}
        </ReactDragListView>
        <button className="btn btn-danger" onClick={() => logout(history)}>
          Logout
      </button>
      </div>
  )
}
export default Boards;