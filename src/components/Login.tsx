import React, { FC } from 'react';
import MicrosoftLogin from "react-microsoft-login";
import { toast } from 'react-toastify';
import { useHistory, Redirect } from 'react-router';
import { isAuthenticated } from '../isAuthenticated';

const Login: FC<any> = () => {

  const history = useHistory();

  const authHandler = (error: any, data: any) => {
    if (data) {
      sessionStorage.setItem('user_data', JSON.stringify(data.accessToken));
      sessionStorage.setItem('user_name', data?.idToken?.name)
      toast.success('Logged In successfully!');
      history.push('/boards')
    }
    if (error) {
      toast.error('Something went wrong!');
    }
  };
  return (
    isAuthenticated() ? <Redirect to="/board" /> :
      <div className="box">
        <div className="container">
          <div className="row">
            <div className="col-lg-4 col-md-4 col-sm-4 col-xs-12 login-card m-auto">
              <div className="box-part text-center">
                <i className="fab fa-microsoft"></i>
                <div className="title">
                  <h4>Microsoft</h4>
                </div>
                <div className="text">
                  <span>Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr scripta ocurreret qui ad.Lorem ipsum dolor sit amet, id quo eruditi eloquentiam. Assum decore te sed. Elitr scripta ocurreret qui ad</span>
                </div>
                <MicrosoftLogin
                  clientId='b7d24755-eca6-4f05-b668-9b6dc5f1cdf5'
                  authCallback={authHandler}
                  buttonTheme={'dark'}
                  className="ms-btn"
                  prompt={"select_account"}
                  withUserData={true}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
  )
}
export default Login;
