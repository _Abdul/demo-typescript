import React from 'react';

const ToDo = ({ todo, index, completeTodo, removeTodo }: any) => {

  return (
    <div className="todo" style={{ textDecoration: todo.isCompleted ? "line-through" : "" }}
      onDragOver={(e: any) => {
        return e.target.className === "todo" ? e.target.classList.add('addSpace') : ''
      }}
      onDragLeave={(e: any) => {
        e.target.classList.remove('addSpace')
      }}
      onDrop={(e: any) => {
        e.target.classList.remove('addSpace')
      }}
    >
      <span>{todo.text}</span>
      <div>
        <button className="btn btn-primary mr-2" onClick={() => completeTodo(index)}>Complete</button>
        <button className="btn btn-danger" onClick={() => removeTodo(index)}>x</button>
      </div>
    </div>
  )
}
export default ToDo;
