import React from 'react';
import { Route, Redirect, useLocation } from 'react-router';
import { isAuthenticated } from '../isAuthenticated';

const PrivateRoute = ({ component: Component, ...rest }: any) => {
  const location = useLocation();
  return (
    <Route {...rest} render={(props: any) => (
      isAuthenticated() ? (
        <Component {...props} />
      ) :
        <Redirect to={{ pathname: '/login', state: { from: location.pathname } }} />
    )} />
  )
}
export default PrivateRoute;
