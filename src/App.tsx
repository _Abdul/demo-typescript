import React from 'react';
import './App.css';
import { Switch, Redirect, Route } from 'react-router';
import Boards from './components/Boards';
import Login from './components/Login';
import PrivateRoute from './components/PrivateRoute';

function App() {
  return (
   <Switch>
     <Route exact path ="/login" component={Login}/> 
     <PrivateRoute path="/board" component={Boards}/>
     <Redirect exact from="/" to="/board"/>
     <Redirect to="/"/>
   </Switch>
  );
}

export default App;
