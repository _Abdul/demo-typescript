export const isAuthenticated = () => {
  const token = sessionStorage.getItem('user_data');
  return token ? token : null;
}

export const logout = (history: any) => {
  sessionStorage.removeItem('user_data');
  history.push('/login')
}